FROM maven:3.5-jdk-8 as builder
LABEL maintainer="Florius <j@eflorius.com.ar>"

WORKDIR /app

# Download deps
ADD pom.xml /app/pom.xml
RUN mvn verify clean --fail-never

ADD . /app

RUN mvn clean package


# Run
FROM openjdk:8-jre-alpine
LABEL maintainer="Florius <j@eflorius.com.ar>"

COPY --from=builder /app/target/core-*.jar /app.war

EXPOSE 8080
CMD ["/usr/bin/java", "-jar", "-Dspring.profiles.active=production", "/app.war"]
