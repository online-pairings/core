package com.ar.florius.online.pairings.core.model;

import com.ar.florius.online.pairings.core.model.wer.Match;
import com.ar.florius.online.pairings.core.model.wer.Person;
import com.ar.florius.online.pairings.core.model.wer.Round;
import com.ar.florius.online.pairings.core.model.wer.WERDocument;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.Month;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class WERParserTest {
    private WERParser werParser;

    @Before
    public void setUp() {
        this.werParser = new WERParser();
    }

    @Test
    public void it_can_parse_a_wer_document_into_an_object() throws FileNotFoundException {
        File file = new File("src/test/resources/demo-finesvizzera.wer");

        Optional<WERDocument> documentOp = werParser.parse(new FileInputStream(file));

        assertThat(documentOp).isPresent();
        WERDocument document = documentOp.orElseThrow(RuntimeException::new);
        assertThat(document)
                .hasFieldOrPropertyWithValue("eventId", "aefb55d1-51b5-4e8a-971a-5024b872bf9f")
                .hasFieldOrPropertyWithValue("startDate", LocalDate.of(2018, Month.FEBRUARY, 22))
                .hasFieldOrPropertyWithValue("format", "MODERN")
                .hasFieldOrPropertyWithValue("title", "Magic: the Gathering")
                .hasFieldOrPropertyWithValue("started", true)
                .hasFieldOrPropertyWithValue("numberOfRounds", 3);

        assertThat(document.getPersons())
                .hasSize(6)
                .extracting(Person::getFirst)
                .containsExactly("Marco", "Federico", "Saverio", "Matteo", "Fabio", "Mirko");

        assertThat(document.getRounds())
                .hasSize(3)
                .extracting(Round::getNumber)
                .containsExactly(1, 2, 3);


        List<Match> matches = document.getRounds().stream().map(Round::getMatches).flatMap(Collection::stream).collect(Collectors.toList());
        assertThat(matches)
                .hasSize(9)
                .extracting(Match::getPerson)
                .containsExactly("3", "9876", "12345", "9876", "1", "12345", "9876", "3", "4");

        assertThat(matches)
                .hasSize(9)
                .extracting(Match::getOpponent)
                .containsExactly("1234456", "1", "4", "3", "4", "1234456", "12345", "1", "1234456");
    }
}
