package com.ar.florius.online.pairings.core.service;


import com.ar.florius.online.pairings.core.utils.PairingEvents;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Java6Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PairingEventRepositoryTest {

    @Autowired
    PairingEventRepository repo;

    @Before
    public void setUp() {
        repo.deleteAll();
    }

    @Test
    public void shouldInsertAndCountData() {
        int toAdd = 2;

        assertThat(repo.count()).isEqualTo(0);
        repo.saveAll(PairingEvents.create(toAdd));
        assertThat(repo.count()).isEqualTo(toAdd);
    }
}