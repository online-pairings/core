package com.ar.florius.online.pairings.core.utils;

import com.ar.florius.online.pairings.core.model.PairingEvent;
import com.github.javafaker.Faker;

import java.time.Clock;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PairingEvents {
    public static List<PairingEvent> create(int howMany) {
        return IntStream.range(0, howMany)
                .mapToObj(i -> create())
                .collect(Collectors.toList());
    }

    public static PairingEvent create() {
        Faker faker = new Faker();
        return new PairingEvent(faker.app().name(), Clock.systemDefaultZone());
    }
}
