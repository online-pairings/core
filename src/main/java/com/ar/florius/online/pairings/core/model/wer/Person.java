package com.ar.florius.online.pairings.core.model.wer;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "person")
public class Person extends Participant {
    @XmlAttribute
    private String id;
    @XmlAttribute
    private String first;
    @XmlAttribute
    private String middle;
    @XmlAttribute
    private String country;

    private Person() {
    }

    public String getFirst() {
        return first;
    }
}
