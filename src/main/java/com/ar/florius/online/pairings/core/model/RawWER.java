package com.ar.florius.online.pairings.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.time.Instant;

@Entity
@Getter
@Setter
public class RawWER {
    @Id
    @GeneratedValue
    private Long id;

    @Lob
    @JsonIgnore
    private String raw;

    private String title;

    @CreationTimestamp
    private Instant created;
}
