package com.ar.florius.online.pairings.core.config.admin;

import org.springframework.core.MethodParameter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@ControllerAdvice
public class WrapperAdvice implements ResponseBodyAdvice {
    @Override
    public boolean supports(MethodParameter returnType, Class converterType) {
        return true;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        if (body instanceof List) {
            return pageList((List)body);
        }
        else if(body instanceof Set) {
            return pageSet((Set)body);
        }
        return body;
    }

    private static <T> Page<T> pageList(List<T> list) {
        if (list == null) {
            return new PageImpl<>(new ArrayList<>());
        }
        return new PageImpl<>(new ArrayList<>(list));
    }

    private static <T> Page<T> pageSet(Set<T> set) {
        if (set == null) {
            return new PageImpl<>(new ArrayList<>());
        }
        return new PageImpl<>(new ArrayList<>(set));
    }
}