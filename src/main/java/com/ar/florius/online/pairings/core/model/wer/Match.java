package com.ar.florius.online.pairings.core.model.wer;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Match extends Participant {
    @XmlAttribute
    private String person;
    @XmlAttribute
    private String opponent;

    private Match() {
    }

    public String getPerson() {
        return person;
    }

    public String getOpponent() {
        return opponent;
    }
}
