package com.ar.florius.online.pairings.core.rest.admin;

import com.ar.florius.online.pairings.core.model.WERParser;
import com.ar.florius.online.pairings.core.model.wer.WERDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;

@RestController
@RequestMapping("/admin/upload")
public class UploadController {
    private final WERParser parser;

    @Autowired
    public UploadController(WERParser parser) {
        this.parser = parser;
    }

    @PostMapping(value = "/new", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    Optional<WERDocument> upload(@RequestParam("file") MultipartFile file) throws IOException {
        return parser.parse(file.getInputStream());
    }

}