package com.ar.florius.online.pairings.core.model;

import com.ar.florius.online.pairings.core.model.wer.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.InputStream;
import java.util.Optional;

@Service
public class WERParser {
    private static Logger logger = LoggerFactory.getLogger(WERParser.class);

    public WERParser() {
    }

    public Optional<WERDocument> parse(InputStream input) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(WERDocument.class, Person.class, Role.class, Team.class, Round.class, Match.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            WERDocument doc = (WERDocument) unmarshaller.unmarshal(input);
            return Optional.of(doc);
        } catch (JAXBException e) {
            logger.error("cant process the .wer", e);
            return Optional.empty();
        }
    }
}
