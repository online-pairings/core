package com.ar.florius.online.pairings.core.rest.admin;

import com.ar.florius.online.pairings.core.model.RawWER;
import com.ar.florius.online.pairings.core.service.admin.RawWERRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Optional;

@RestController
@RequestMapping("/admin/wer")
public class RawWERController {

    private final RawWERRepository repo;
    private final ObjectMapper om;

    @Autowired
    public RawWERController(RawWERRepository repo, ObjectMapper om) {
        this.repo = repo;
        this.om = om;
    }
 
    @GetMapping("")
    Page<RawWER> list(@RequestParam(required = false, name = "filter") String filter, Pageable page) throws IOException {
        return repo.findAll(Example.of(om.readValue(filter, RawWER.class)), page);
    }

    @PostMapping("")
    RawWER save(@RequestBody RawWER wer) {
        return repo.save(wer);
    }

    @PutMapping("/{id}")
    RawWER update(@PathVariable Long id, @RequestBody RawWER wer) {
        wer.setId(id);
        return repo.save(wer);
    }

    @GetMapping("/{id}")
    Optional<RawWER> find(@PathVariable Long id) {
        return repo.findById(id);
    }
}    