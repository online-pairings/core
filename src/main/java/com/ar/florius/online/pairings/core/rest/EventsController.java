package com.ar.florius.online.pairings.core.rest;

import com.ar.florius.online.pairings.core.model.PairingEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Clock;
import java.util.Arrays;
import java.util.List;

@RestController
public class EventsController {
    private final Clock clock;

    @Autowired
    public EventsController(Clock clock) {
        this.clock = clock;
    }

    @GetMapping("/events")
    List<PairingEvent> list() {
        return Arrays.asList(new PairingEvent("Features 1", clock)
                , new PairingEvent("Features 2", clock)
                , new PairingEvent("Features 3", clock));
    }
}    