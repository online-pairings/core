package com.ar.florius.online.pairings.core.model.wer;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Role extends Participant {
    @XmlAttribute
    private String type;
    @XmlAttribute
    private String cd;

    private Role() {
    }
}
