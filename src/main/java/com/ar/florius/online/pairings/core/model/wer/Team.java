package com.ar.florius.online.pairings.core.model.wer;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Team extends Participant {
    @XmlAttribute
    private String id;
    @XmlAttribute
    private String name;

    private Team() {
    }
}
