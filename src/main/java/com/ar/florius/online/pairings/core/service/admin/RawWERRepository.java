package com.ar.florius.online.pairings.core.service.admin;


import com.ar.florius.online.pairings.core.model.RawWER;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RawWERRepository extends JpaRepository<RawWER, Long> {
}
