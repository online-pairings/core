package com.ar.florius.online.pairings.core.service;

import com.ar.florius.online.pairings.core.model.PairingEvent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PairingEventRepository extends JpaRepository<PairingEvent, String> {
}