package com.ar.florius.online.pairings.core.model.wer;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@XmlRootElement
public class Round extends Participant {
    @XmlAttribute
    private Integer number;
    private LocalDateTime date;
    @XmlElement(name = "match")
    private List<Match> matches;

    private Round() {
    }

    @XmlAttribute(name = "date")
    private void setDate(String date) {
        this.date = LocalDateTime.parse(date, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss"));
    }

    public Integer getNumber() {
        return number;
    }

    public List<Match> getMatches() {
        return matches;
    }
}
