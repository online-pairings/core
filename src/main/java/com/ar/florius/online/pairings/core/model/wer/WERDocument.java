package com.ar.florius.online.pairings.core.model.wer;

import org.apache.commons.lang3.BooleanUtils;

import javax.xml.bind.annotation.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@XmlRootElement(name = "event")
public class WERDocument {
    @XmlAttribute(name = "eventguid")
    private String eventId;
    private LocalDate startDate;
    @XmlAttribute
    private String format;
    @XmlAttribute
    private String title;
    private Boolean started;
    @XmlAttribute(name = "numberofrounds")
    private Integer numberOfRounds;
    @XmlAnyElement(lax = true)
    @XmlElementWrapper(name = "participation")
    private List<Participant> participants;
    @XmlElementWrapper(name = "matches")
    @XmlElement(name = "round")
    private List<Round> rounds;

    public WERDocument() {
    }

    @XmlAttribute(name = "startdate")
    public void setStartDate(String startDate) {
        this.startDate = LocalDate.parse(startDate, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
    }

    @XmlAttribute(name = "isstarted")
    private void setStarted(String started) {
        this.started = BooleanUtils.toBoolean(started);
    }

    public List<Person> getPersons() {
        return participants.stream().filter(x -> x instanceof Person).map(person -> (Person) person).collect(Collectors.toList());
    }

    public List<Round> getRounds() {
        return rounds;
    }
}
