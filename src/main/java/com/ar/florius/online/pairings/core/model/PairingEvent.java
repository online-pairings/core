package com.ar.florius.online.pairings.core.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.Clock;
import java.time.Instant;

@Entity
@Getter
@Setter
public class PairingEvent {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private Instant updated;
    private Instant created;

    public PairingEvent(String name, Clock clock) {
        this.name = name;
        this.created = Instant.now(clock).minusSeconds(500);
        this.updated = Instant.now(clock);
    }
}
